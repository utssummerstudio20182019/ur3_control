# ur3_control

## Pre-requisites
ROS Kinetic

Gazebo 7.0

$ sudo apt-get install ros-kinetic-universal-robot

$ sudo apt-get install ros-kinetic-ur-gazebo

$ sudo apt-get install ros-kinetic-moveit


## Installation
$ cd ~/

$ mkdir -p ~/catkin_ws/src

$ cd ~/catkin_ws/src

$ git clone <this repo>

$ cd ~/catkin_ws

$ catkin_make


## Running the Simulation
Remember to source the workspace before running each command

Terminal_1:~$ roslaunch ur3_control ur3_moveit.launch

Terminal_2:~$ roslaunch ur3_control move_group_interface_tutorial.launch
